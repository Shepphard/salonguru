import express from 'express';
import bodyParser from 'body-parser'
import { salonRouter } from './routes/salons';
import { authRouter } from './routes/auth';

const app = express();
const port = 3000;

app.use(bodyParser.json())

app.use('/salons', salonRouter());
app.use('/auth', authRouter());

app.listen(port, () => {
  console.log(`Salonguru API is running on port ${port}.`);
});
