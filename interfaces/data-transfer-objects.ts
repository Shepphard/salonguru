/**
 * The DTO for returning salon information
 */
export interface SalonDTO {
  /**
   * The salons id
   */
  id: number;
  /**
   * The name of the salon
   */
  name: string;
  /**
   * The url of the homepage
   */
  url: string;
  /**
   * The url to the salons image
   */
  imageUrl: string;
  /**
   * The full address of the salon in a single string
   */
  fullAddress: string;
  /**
   * The number of ratings
   */
  numberOfRatings: number;
  /**
   * The average rating, if any ratings are available.
   */
  averageRating?: number;
}

/**
 * The DTO for returning a Rating
 */
export interface RatingDTO {
  /**
   * The unique id of the rating
   */
  id: number;
  /**
   * The value of the rating
   */
  rating: number;
  /**
   * The comment added to the rating
   */
  comment: string;
}

/**
 * The DTO used for creating a rating
 */
export interface CreateRatingDTO {
  /**
   *  The value of the rating
   */
  rating: number;
  /**
   * The comment added to the rating
   */
  comment: string;
}

/**
 * The DTO used for editing a rating. It extends the CreateRatingDTO
 */
export interface EditRatingDTO extends CreateRatingDTO {
  /**
   * The id of the rating to be changed
   */
  ratingId: number;
}

/**
 * The DTO used to delete a rating
 */
export interface DeleteRatingDTO {
  /**
   * The id of the rating to be deleted
   */
  ratingId: number;
}

/**
 * The DTO for the login
 */
export interface LoginDTO {
  /**
   * The username as a string
   */
  username: string;
  /**
   * The unencrypted password
   */
  password: string;
}
