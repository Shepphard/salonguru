/**
 * Interface for the Salon Entities in the database
 */
 export interface Salon {
    /**
     * The unique ID of a Salon
     */
    id: number,
    /**
     * The name of a salon
     */
    name: string,
    /**
     * The url of the salons website
     */
    url: string,
    /**
     * The image of the salon
     */
    salon_image: string,
    /**
     * The address line of the salon
     */
    address: string,
    /**
     * The postal code of the salon
     * TODO: It is a number, could be changed to string in order to allow
     * letters in the postal code (like in GB and IE)
     */
    postal_code: number,
    /**
     * The city of the salon
     */
    city: string
}

/**
 * Interface for the ratings
 */
export interface Rating {
    /**
     * Unique Id of the rating
     */
    id: number,
    /**
     * Id of the salon the id belongs to
     */
    salonId: number,
    /**
     * The id of the user that created the rating
     */
    userId: number,
    /**
     * The actual rating (number between 1 and 5)
     */
    rating: number,
    /**
     * An optional comment to the rating
     */
    comment?: string
}

/**
 * Interface for the user entity
 */
export interface User {
    /**
     * The id of the user
     */
    id: number,
    /**
     * The username
     */
    username: string,
    /**
     * The password
     * As this is a proof of concept, the password is stored plain text.
     */
    password: string
}