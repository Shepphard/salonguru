# Salonguru

Install all the dependencies using 

```yarn install (npm install)```

The API can be started using  

```yarn start (npm start)```

and can be accessed via localhost:3000

## Salons-Endpoint

- **GET** /salons
  
Return a list of all the salons in the system

- **GET** /salons/:salonId

Return the salon with id *salonId*

- **GET** /salons/:salonId/ratings

Get the ratings of the salon with id *salonId*

- **POST** /salons/:salonId/ratings (AUTH REQUIRED)

Add a rating to the salon with id *:salonId*

- **PUT** /salons/:salonId/ratings (AUTH REQUIRED)

Edit a rating of the salon with id *:salonId*

- **DELETE** /salons/:salonId/ratings

Delete a rating of the salon with id *:salonId*

## Auth-Endpoint

- **POST** /auth

Login a User

## Explanations on implementation

- The authentication proces is very basic and in no way a correct way to implement it. It basically just returns the id of the user after successful authentication. This id is then also used to identify the user during a POST/PUT/DELETE of a rating.
- After working on the project I realized I misread a part of the task. I only created one API instead of the requested two.
- There are some repetitions in the code, which can be made easier with refactorization of the code.
- I did not use a database, therefore the ratings will reset after each restart of the api server.
- The exchange of information works mainly with Data Transfer Object (DTOs).
- I used Typescript.
- The nodemon package was used for the server to restart itself automatically when a change was made to a file.
- During development, the tests were performed using Postman. Example requests are included.
- Some endpoints expose the entity declaration as a response. In production, this should not be done in order to avoid unlawful people discovering the database layout.