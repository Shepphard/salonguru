import express from "express";
import { LoginDTO } from "../interfaces/data-transfer-objects";
import { users } from "../resources/users";

/**
 * The router with all the auth nodes
 * @returns the authorization router
 */
export const authRouter = () => {
  let router = express.Router();

  //load all the users on startup
  const userInfos = users;

  // POST Node to login.
  router.post("/", (req, res) => {
    //Cast the request body to the DTO
    const loginDTO: LoginDTO = req.body;

    //Check if all required parameters are available.
    if (loginDTO.username && loginDTO.password) {
      //Get the user that is trying to login
      const user = userInfos.find(
        (userInfo) => userInfo.username === loginDTO.username
      );

      //If user exists and password is correct, return the user Id. It will be used as key to identify a request on ratings
      if (user && user.password === loginDTO.password) {
        res.send({ id: user.id });
      } else {
        //Send Unauthorized else
        res.sendStatus(401);
      }
    } else {
      //If not all params given -> Bad Request
      res.sendStatus(400);
    }
  });

  return router;
};
