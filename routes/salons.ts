import express from "express";
import {
  SalonDTO,
  RatingDTO,
  EditRatingDTO,
  CreateRatingDTO,
  DeleteRatingDTO,
} from "../interfaces/data-transfer-objects";
import { Rating, Salon } from "../interfaces/models";
import { salons } from "../resources/salons-list";
import { users } from "../resources/users";

export const salonRouter = () => {
  //List of ratings
  let ratings: Rating[] = [];
  let router = express.Router();

  //Get the salon by ID
  const getSalon = (id: number) => {
    return salons.find((salon) => {
      return salon.id === id;
    });
  };

  //Get the average rating and number of ratings per salon
  const averageRatingForSalon = (salon: Salon) => {
    //Only get ratings for salon
    const salonRatings = ratings.filter((rating) => {
      return rating.salonId === salon.id;
    });

    //If no salonrating available, return object with numberOfRatings 0, otherwise calculate average and return average and number of ratings.
    if (salonRatings.length > 0) {
      //Average calculation
      const average =
        salonRatings
          .map((currentSalon) => currentSalon.rating) //Only get the rating property
          .reduce((a, b) => a + b) / salonRatings.length;

      return {
        numberOfRatings: salonRatings.length,
        average,
      };
    } else {
      return {
        numberOfRatings: 0,
      };
    }
  };
  /**********
   *
   *  MIDDLEWARES
   *
   **********/

  //Check if a user is authenticated and user header is available
  /****IMPORTANT
   * This is just a basic way to authentify a user. This method would actually check a JWT token for it's validity
   * and based on that info identify the user. This is pure PoC Code
   */
  const checkAuth = (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    //Get the user ID
    let userId = parseInt(req.headers.user as string);

    //If the user is correct, continue to next function
    if (users.findIndex((user) => user.id === userId) > -1) {
      next();
    } else {
      res.sendStatus(401);
    }
  };

  // Check if a user can change the requested rating
  const canChangeRating = (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    //Cast the rating to the DTO and get the User ID
    const ratingDTO = req.body as EditRatingDTO;
    let userId = parseInt(req.headers.user as string);

    //Get the rating that will be modified
    const rating = ratings.find((r) => r.id === ratingDTO.ratingId);

    //If no rating was found, return 404
    if (!rating) {
      res.sendStatus(404);
      return;
    }

    //Check if the userId of the ratings creator is the same as current user.
    if (rating.userId === userId) {
      next();
    } else {
      res.sendStatus(401);
    }
  };

  //Get all the salons
  router.get("/", (req, res) => {
    let results: SalonDTO[] = [];

    //Iterate through all the salons
    salons.forEach((salon) => {
      // Get the average Rating for the salon
      const rating = averageRatingForSalon(salon);

      //Add dto to the results array
      results.push({
        id: salon.id,
        name: salon.name,
        url: salon.url,
        imageUrl: salon.salon_image,
        fullAddress: `${salon.address}, ${salon.postal_code} ${salon.city}`,
        numberOfRatings: rating.numberOfRatings,
        averageRating: rating.average,
      });
    });

    res.send(results);
  });

  //Get a salon with a specific ID
  router.get("/:salonId", (req, res) => {
    //Parse the string from the request to Int
    let salonId = parseInt(req.params.salonId);

    //If given id is not a number, end in 500 Internal Server Error status and end
    if (isNaN(salonId)) {
      console.error("Could not parse salonId to Integer value");
      res.sendStatus(500);
      return;
    }

    //Get the salon
    var salon = getSalon(salonId);

    //If salon exists, send answer DTO, if not send 404 Not Found Status
    if (salon) {
      //Get the rating
      const rating = averageRatingForSalon(salon);

      res.send({
        id: salon.id,
        name: salon.name,
        url: salon.url,
        imageUrl: salon.salon_image,
        fullAddress: `${salon.address}, ${salon.postal_code} ${salon.city}`,
        numberOfRatings: rating.numberOfRatings,
        averageRating: rating.average,
      });
    } else {
      res.sendStatus(404);
    }
  });

  //Get all the ratings for a specific salon
  router.get("/:salonId/ratings", (req, res) => {
    //Parse the string from the request to Int
    let salonId = parseInt(req.params.salonId);

    //If given id is not a number, end in 500 Internal Server Error status and end
    if (isNaN(salonId)) {
      console.error("Could not parse salonId to Integer value");
      res.sendStatus(500);
      return;
    }

    //get all the ratings and map them to the DTO
    const results = ratings
      .filter((rating) => {
        return rating.salonId === salonId;
      })
      .map((rating) => {
        let r: RatingDTO = {
          id: rating.id,
          rating: rating.rating,
          comment: rating.comment,
        };
        return r;
      });

    res.send(results);
  });

  //Create a rating for a specific salon
  router.post(
    "/:salonId/ratings",
    (req, res, next) => checkAuth(req, res, next),
    (req, res) => {
      let userId = parseInt(req.headers.user as string);
      //Parse the string from the request to Int
      let salonId = parseInt(req.params.salonId);

      //If given id is not a number, end in 500 Internal Server Error status and end
      if (isNaN(salonId)) {
        console.error("Could not parse salonId to Integer value");
        res.sendStatus(500);
        return;
      }

      //Cast the body to the create rating dto
      const ratingDTO = req.body as CreateRatingDTO;

      //Check if any rating is already available for the currently logged user
      const ratingAvailable =
        ratings.findIndex(
          (rating) => rating.salonId === salonId && rating.userId === userId
        ) > -1;

      //If yes, answer with a conflict code. Could be another one as well, conflict is not the best choice.
      if (ratingAvailable) {
        //Answer with conflict
        res.sendStatus(409);
        return;
      }

      //Automatically adjust if unsupported values are coming in.
      if (ratingDTO.rating > 5) {
        ratingDTO.rating = 5;
      }
      if (ratingDTO.rating < 1) {
        ratingDTO.rating = 1;
      }

      //Get the biggest id in the array and add 1. If the array is empty, this will always result in 1.
      const newId =
        ratings.length > 0
          ? ratings.reduce((prev, current) => {
              return prev.id > current.id ? prev : current;
            }).id + 1
          : 1;

      //Create a new rating object
      const rating: Rating = {
        id: newId,
        salonId: parseInt(req.params.salonId),
        userId,
        rating: ratingDTO.rating,
        comment: ratingDTO.comment,
      };

      ratings.push(rating);

      //Return the new entity. Return could e sth else.
      res.send(ratings);
    }
  );

  //Edit the rating
  router.put(
    "/:salonId/ratings",
    (req, res, next) => checkAuth(req, res, next),
    (req, res, next) => canChangeRating(req, res, next),
    (req, res) => {
      let userId = parseInt(req.headers.user as string);

      //Cast the body to the editrating dto
      let editRatingDTO: EditRatingDTO = req.body as EditRatingDTO;

      //Get the rating object
      const rating = ratings.find(
        (r) => r.id === editRatingDTO.ratingId && r.userId === userId
      );

      //If none found return 404
      if (!rating) {
        res.sendStatus(404);
        return;
      }

      //Automatically adjust if unsupported values are coming in.
      if (editRatingDTO.rating > 5) {
        editRatingDTO.rating = 5;
      }
      if (editRatingDTO.rating < 1) {
        editRatingDTO.rating = 1;
      }

      //Exchange the values in the object with the new ones
      rating.rating = editRatingDTO.rating;
      rating.comment = editRatingDTO.comment;

      res.send(rating);
    }
  );

  //Delete a rating
  router.delete(
    "/:salonId/ratings",
    (req, res, next) => checkAuth(req, res, next),
    (req, res, next) => canChangeRating(req, res, next),
    (req, res) => {
        let userId = parseInt(req.headers.user as string);
        //Cast body to 
      const deleteRatingDTO = req.body as DeleteRatingDTO;

      //Get the index of the rating with the userID
      const ratingIndex = ratings.findIndex(
        (rating) => rating.id === deleteRatingDTO.ratingId && rating.userId === userId
      );

      //Remove the rating from the list
      const deletedRating = ratings.splice(ratingIndex, 1);

      if (deletedRating.length > 0) {
        res.send(deletedRating);
      } else {
        res.send("Nothing was deleted");
      }
    }
  );

  return router;
};
