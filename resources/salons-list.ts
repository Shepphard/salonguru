import { Salon } from "../interfaces/models";

export const salons: Salon[] = [
  {
    "id": 1,
    "name": "Hair Today Dye Tomorrow",
    "url": "https://eepurl.com/nunc/donec/quis/orci.html",
    "salon_image": "http://dummyimage.com/198x100.png/dddddd/000000",
    "address": "432 Katie Circle",
    "postal_code": 6196,
    "city": "Tunggaoen Timur"
  },
  {
    "id": 2,
    "name": "A Breath of FresHair",
    "url": "http://cloudflare.com/rhoncus/mauris/enim/leo/rhoncus/sed.xml",
    "salon_image": "http://dummyimage.com/136x100.png/ff4444/ffffff",
    "address": "2628 Maryland Court",
    "postal_code": 1277,
    "city": "Dārāb"
  },
  {
    "id": 3,
    "name": "Ooooh Girl, who did Your Hair?",
    "url": "https://shutterfly.com/nullam/varius/nulla/facilisi/cras/non.jsp",
    "salon_image": "http://dummyimage.com/143x100.png/ff4444/ffffff",
    "address": "3307 Merrick Center",
    "postal_code": 2643,
    "city": "Limoges"
  },
  {
    "id": 4,
    "name": "Hairitage",
    "url": "https://blinklist.com/lorem/ipsum.html",
    "salon_image": "http://dummyimage.com/135x100.png/5fa2dd/ffffff",
    "address": "7 Summer Ridge Avenue",
    "postal_code": 4850,
    "city": "Puerto Iguazú"
  },
  {
    "id": 5,
    "name": "Hairport",
    "url": "https://pbs.org/quisque/ut.xml",
    "salon_image": "http://dummyimage.com/168x100.png/5fa2dd/ffffff",
    "address": "43 Sunfield Hill",
    "postal_code": 2373,
    "city": "El Fahs"
  },
  {
    "id": 6,
    "name": "Cut-N-Edge",
    "url": "http://webnode.com/suspendisse/potenti/cras/in/purus.js",
    "salon_image": "http://dummyimage.com/149x100.png/cc0000/ffffff",
    "address": "392 Chive Road",
    "postal_code": 2456,
    "city": "Khānewāl"
  },
  {
    "id": 7,
    "name": "Anita Haircut",
    "url": "http://com.com/iaculis/diam.js",
    "salon_image": "http://dummyimage.com/144x100.png/ff4444/ffffff",
    "address": "775 Village Pass",
    "postal_code": 9783,
    "city": "Kuala Terengganu"
  },
  {
    "id": 8,
    "name": "Ryanhair",
    "url": "http://gravatar.com/id/luctus/nec/molestie/sed/justo/pellentesque.jsp",
    "salon_image": "http://dummyimage.com/119x100.png/cc0000/ffffff",
    "address": "13 Stang Parkway",
    "postal_code": 9892,
    "city": "Santa Maria"
  },
  {
    "id": 9,
    "name": "Hair We Go!",
    "url": "https://domainmarket.com/ante.png",
    "salon_image": "http://dummyimage.com/104x100.png/dddddd/000000",
    "address": "0 Service Way",
    "postal_code": 8640,
    "city": "Talcahuano"
  },
  {
    "id": 10,
    "name": "I’ll Cut You",
    "url": "http://ted.com/nibh/in/lectus/pellentesque/at/nulla.png",
    "salon_image": "http://dummyimage.com/247x100.png/5fa2dd/ffffff",
    "address": "09 Ronald Regan Plaza",
    "postal_code": 8297,
    "city": "Tazhuang"
  }
]