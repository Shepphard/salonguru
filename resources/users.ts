import { User } from "../interfaces/models";

export const users: User[] = [
    {
        id: 100,
        username: 'philippe',
        password: 'kayser'
    },
    {
        id: 200,
        username: 'tom',
        password: 'michels'
    },
    {
        id: 300,
        username: 'arne',
        password: 'tempelhof'
    }
]